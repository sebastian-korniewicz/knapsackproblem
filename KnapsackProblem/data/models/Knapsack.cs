﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.data.models
{
    public class Knapsack
    {
        private int capacity;
        public int Capacity => capacity;

        public List<Item> Items { get; }

        public Knapsack(int capacity, List<Item> items)
        {
            this.capacity = capacity;
            this.Items = items;
        }

        public String GetInfo()
        {
            String info = "";
            info += "Capacity: " + capacity + "\n";
            info += "Items:\n" +
                    "id\tweight\tvalue\n";
            int iter = 0;
            foreach (var item in Items)
            {
                info += iter + "\t" + item.GetInfo() + "\n";
                iter++;
            }
            return info;
        }
    }
}
