﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.data.models
{
    public class KnapsackResult
    {
        private List<Item> items;
        private int sumOfValue;

        public List<Item> Items => items;
        public int SumOfValue => sumOfValue;

        public KnapsackResult(List<Item> items, int sumOfValue)
        {
            this.items = items;
            this.sumOfValue = sumOfValue;
        }

        public String GetInfo()
        {
            String info = "Sum of value: " + sumOfValue;
            info += "\nweight\tvalue\n";

            for (int i = 0; i < items.Count; i++)
            {
                info += items[i].GetInfo() + "\n";
            }

            return info;

        }
    }
}
