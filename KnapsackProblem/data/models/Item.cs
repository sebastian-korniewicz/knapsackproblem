﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.data.models
{
    public class Item
    {
        private int weight;
        private int value;

        public Item(int weight, int value)
        {
            this.weight = weight;
            this.value = value;
        }

        public String GetInfo()
        {
            String info = weight + "\t" + value;
            return info;
        }

        public int Weight => weight;

        public int Value => value;
    }
}
