﻿using KnapsackProblem.data.models;
using System;
using System.Collections.Generic;

namespace KnapsackProblem.data.generator
{
    public class Generator
    {
        private Random random;

        public Generator()
        {
            random = new Random();
        }

        public Knapsack GenerateKnapsack(int amountOfItems, int range, int capacity, DataType dataType)
        {
            Knapsack knapsack;
            List<Item> items;
            switch (dataType)
            {
                case DataType.UncorrelatedDataInstances:
                    items = uncorrelatedData(amountOfItems, range);
                    break;
                case DataType.WeaklyCorrelatedInstances:
                    items = weaklyCorrelated(amountOfItems, range);
                    break;
                case DataType.StronglyCorrelatedInstances:
                    items = stronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.InverseStronglyCorrelatedInstances:
                    items = inverseStronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.AlmostStronglyCorrelatedInstances:
                    items = almostStronglyCorrelatedInstances(amountOfItems, range);
                    break;
                case DataType.SubsetSumInstances:
                    items = subsetSumInstances(amountOfItems, range);
                    break;
                default:
                    return null;
            }

            knapsack = new Knapsack(capacity, items);
            return knapsack;
        }

        private List<Item> uncorrelatedData(int amountOfItems, int range)
        {
            List<Item> items = new List<Item>();
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = random.Next(1, range);
                items.Add(new Item(weight, value));
            }
            return items;
        }

        private List<Item> weaklyCorrelated(int amountOfItems, int range)
        {
            List<Item> items = new List<Item>();
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = random.Next(weight - range / 10, weight + range / 10);
                items.Add(new Item(weight, value));
            }
            return items;
        }

        private List<Item> stronglyCorrelatedInstances(int amountOfItems, int range)
        {
            List<Item> items = new List<Item>();
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = weight + range / 10;
                items.Add(new Item(weight, value));
            }
            return items;
        }

        private List<Item> inverseStronglyCorrelatedInstances(int amountOfItems, int range)
        {
            List<Item> items = new List<Item>();
            for (int i = 0; i < amountOfItems; i++)
            {
                int value = random.Next(1, range);
                int weight = value + range / 10;
                items.Add(new Item(weight, value));
            }
            return items;
        }

        private List<Item> almostStronglyCorrelatedInstances(int amountOfItems, int range)
        {
            List<Item> items = new List<Item>();
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = random.Next(weight + range / 10 - range / 500, weight + range / 10 + range / 500);
                items.Add(new Item(weight, value));
            }
            return items;
        }

        private List<Item> subsetSumInstances(int amountOfItems, int range)
        {
            List<Item> items = new List<Item>();
            for (int i = 0; i < amountOfItems; i++)
            {
                int weight = random.Next(1, range);
                int value = weight;
                items.Add(new Item(weight, value));
            }
            return items;
        }
    }
}
