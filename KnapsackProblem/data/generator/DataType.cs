﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.data.generator
{
    public enum DataType
    {
        UncorrelatedDataInstances,
        WeaklyCorrelatedInstances,
        StronglyCorrelatedInstances,
        InverseStronglyCorrelatedInstances,
        AlmostStronglyCorrelatedInstances,
        SubsetSumInstances,
        UncorrelatedInstancesWithSimilarweights
    }
}
