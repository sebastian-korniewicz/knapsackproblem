﻿using KnapsackProblem.data.models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KnapsackProblem.solverInterfaces
{
    interface IDiscreteKnapsackSolver
    {
        KnapsackResult Solve(Knapsack knapsack);
        String GetName();
    }
}
