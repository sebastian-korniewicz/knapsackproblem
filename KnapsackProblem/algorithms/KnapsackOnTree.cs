﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.data.models;
using KnapsackProblem.solverInterfaces;

namespace KnapsackProblem.algorithms
{
    public class KnapsackOnTree : IDiscreteKnapsackSolver
    {
        public string GetName()
        {
            throw new NotImplementedException();
        }

        public KnapsackResult Solve(Knapsack knapsack)
        {
            List<Item> items = knapsack.Items.OrderBy(o => o.Weight).ToList();
            //items.Insert(0, new Item(0,0));

            for (int i = 0; i < items.Count; i++)
            {
                Console.WriteLine(items[i].GetInfo());
            }

            int t = -1;
            int sumOfWeight = 0;
            bool end = false;
            do
            {
                sumOfWeight += items[++t].Weight;
            } while (sumOfWeight<=knapsack.Capacity);

            // init max node
            Node maxNode = new Node();
            maxNode.Weight = sumOfWeight - items[t].Weight;
            maxNode.FirstRightChild = t;
            maxNode.Level = t;
            maxNode.RecommendationNode = null;
            maxNode.Lipr = 'A';
            maxNode.Prompr = 0;
            maxNode.Promle = 0;
            for (int i = 0; i < t; i++)
            {
                maxNode.Value += items[i].Value;
            }

            Node firstA = new Node();
            Node lastA = new Node();
            firstA.Lipr = 'A';
            Node.CreateList(ref firstA, ref lastA);

            Node firstB = new Node();
            Node lastB = new Node();
            Node.CreateList(ref firstB, ref lastB);

            Node firstC = new Node();
            Node lastC = new Node();
            Node.CreateList(ref firstC, ref lastC);

            Node firstD = new Node();
            Node lastD = new Node();
            Node.CreateList(ref firstD, ref lastD);

            Node.Append(ref lastA, maxNode.Weight, maxNode.Value, maxNode.FirstRightChild, maxNode.Level, ref (maxNode.RecommendationNode), 'A', 0);

            var d = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            Console.WriteLine(d);
            var x = GC.GetTotalMemory(false) / 1024;

            for (int poz = t-1; poz>=0; poz--)
            {
                x = GC.GetTotalMemory(false) / 1024;
                Node.Find(ref firstA, ref lastB, ref lastD, ref maxNode,items.Count, knapsack.Capacity, poz, items);
                x = GC.GetTotalMemory(false) / 1024;
                Node.Find(ref firstC, ref lastB, ref lastD, ref maxNode, items.Count, knapsack.Capacity, poz, items);
                x = GC.GetTotalMemory(false) / 1024;
                Node.Connect(ref lastA, ref firstB, ref lastB);
                x = GC.GetTotalMemory(false) / 1024;
                Node.Connect(ref lastC, ref firstD, ref lastD);
                Console.WriteLine(DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond - d);
                x = GC.GetTotalMemory(false) / 1024;
                GC.Collect();
                d = DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond;
            }
            Console.WriteLine(t);

            maxNode.PrintResult();
            return null;
        }

    }

    class Node
    {
        private int weight;
        private int value;
        private int firstRightChild;
        private int level;
        private Node successor = null;
        private Node predecessor = null;
        public Node RecommendationNode = null;
        private char lipr;
        private int prompr;
        private int promle;

        public Node()
        {
        }

        public void PrintResult()
        {
            Console.WriteLine("Waga: " + weight);
            Console.WriteLine("Wartosc: " + value);
        }
        static public void CreateList(ref Node firstNode, ref Node secondNode)
        {
            firstNode.Successor = secondNode;
            secondNode.Predecessor = firstNode;
        }

        static public void Connect(ref Node konA, ref Node poczB, ref Node konB)
        {
            if (poczB.Successor.Successor!=null)
            {
                konB.Predecessor.Successor = konA;
                poczB.Successor.Predecessor = konA.Predecessor;
                konA.Predecessor.Successor = poczB.Successor;
                konA.Predecessor = konB.Predecessor;
                poczB.Successor = konB;
                konB.Predecessor = poczB;

                konA.Successor = null;
                poczB.Predecessor = null;
                konB.Successor = null;
            }
        }
        static public void Find(ref Node poczA, ref Node konB, ref Node konD, ref Node MaxWar, int n, int P, int poz,
            List<Item> items)
        {
            int aktwag; int aktwar; int wskwag;
            Node koleA = new Node();
            koleA = poczA.Successor;

            //koleA.Level = 20;
            //koleA.Level = 20;
            while (koleA.Successor!=null)
            {
                wskwag = koleA.FirstRightChild;
                if (wskwag<n)
                {
                    aktwag = koleA.Weight - items[poz].Weight;
                    aktwar = koleA.Value - items[poz].Value;
                    do
                    {
                        aktwag = aktwag + items[wskwag].Weight;
                        aktwar = aktwar + items[wskwag].Value;
                        if (aktwag<=P)
                        {
                            Append(ref konB, aktwag, aktwar, wskwag+1, koleA.Level, ref koleA, 'A', poz);
                            if (MaxWar.Value < aktwar)
                            {
                                MaxWar.Weight = aktwag;
                                MaxWar.Value = aktwar;
                                MaxWar.FirstRightChild = wskwag + 1;
                                MaxWar.Level = koleA.Level;
                                MaxWar.RecommendationNode = koleA;
                                MaxWar.lipr = 'A';
                                MaxWar.prompr = poz;
                                MaxWar.promle = wskwag;
                            }

                            aktwag = aktwag - items[wskwag].Weight;
                            aktwar = aktwar - items[wskwag].Value;
                        }
                        else
                        {
                            if (poz>0)
                            {
                                aktwag = aktwag - items[wskwag].Weight;
                                aktwar = aktwar - items[wskwag].Value;
                                Append(ref konD, aktwag, aktwar, wskwag, koleA.Level-1, ref koleA, 'C', poz);
                            }
                        }

                        wskwag++;

                    } while ((aktwag<=P)&&(wskwag<n));
                }

                koleA = koleA.Successor;
            }
        }
        static public void Append(ref Node lastNode, int weight, int value, int pipr, int powe, ref Node prom,
            char lipr, int prompr)
        {
            Node newNode = new Node();
            newNode.Weight = weight;
            newNode.Value = value;
            newNode.FirstRightChild = pipr;
            newNode.Level = powe;
            newNode.RecommendationNode = prom;
            newNode.Prompr = prompr;
            if ('A' == lipr)
                newNode.promle = pipr - 1;
            else
                newNode.promle = -1;

            newNode.Successor = lastNode;
            newNode.Predecessor = lastNode.Predecessor;
            lastNode.Predecessor.Successor = newNode;
            lastNode.Predecessor = newNode;
        }

        public int Weight
        {
            get => weight;
            set => weight = value;
        }

        public int Value
        {
            get => value;
            set => this.value = value;
        }

        public int FirstRightChild
        {
            get => firstRightChild;
            set => firstRightChild = value;
        }

        public int Level
        {
            get => level;
            set => level = value;
        }

        public Node Successor
        {
            get => successor;
            set => successor = value;
        }

        public Node Predecessor
        {
            get => predecessor;
            set => predecessor = value;
        }

        //public Node RecommendationNode
        //{
        //    get => recommendationNode;
        //    set => recommendationNode = value;
        //}

        public char Lipr
        {
            get => lipr;
            set => lipr = value;
        }

        public int Prompr
        {
            get => prompr;
            set => prompr = value;
        }

        public int Promle
        {
            get => promle;
            set => promle = value;
        }

    } 
}
