﻿using KnapsackProblem.data.models;
using KnapsackProblem.solverInterfaces;
using System.Collections.Generic;

namespace KnapsackProblem.algorithms
{
    public class DynamicProgramming : IDiscreteKnapsackSolver
    {
        public string GetName()
        {
            return "Dynamic programming";
        }

        public KnapsackResult Solve(Knapsack knapsack)
        {
            int[,] sumOfValues = new int[knapsack.Items.Count + 1, knapsack.Capacity + 1];
            int[,] lastItem = new int[knapsack.Items.Count + 1, knapsack.Capacity + 1];

            for (int k = 0; k < knapsack.Items.Count + 1; k++)
            {
                lastItem[k, 0] = -1;
            }
            for (int k = 0; k < knapsack.Capacity + 1; k++)
            {
                lastItem[0, k] = -1;
            }

            for (int i = 1; i <= knapsack.Items.Count; i++)
            {
                int idItem = i - 1;

                for (int j = 1; j <= knapsack.Capacity; j++)
                {
                    if (knapsack.Items[idItem].Weight > j)
                    {
                        sumOfValues[i, j] = sumOfValues[i - 1, j];
                        lastItem[i, j] = lastItem[i - 1, j];
                    }
                    else
                    {
                        int value1 = sumOfValues[i - 1, j];
                        int value2 = sumOfValues[i - 1, j - knapsack.Items[idItem].Weight] + knapsack.Items[idItem].Value;
                        if (value1 > value2)
                        {
                            sumOfValues[i, j] = value1;
                            lastItem[i, j] = lastItem[i - 1, j];
                        }
                        else
                        {
                            sumOfValues[i, j] = value2;
                            lastItem[i, j] = i;
                        }
                    }
                }
            }

            //for (int i = 0; i <= knapsack.Capacity; i++)
            //{
            //    for (int j = 0; j <= knapsack.Items.Count; j++)
            //    {
            //        Console.Write(sumOfValues[j, i] + "\t");
            //    }
            //    Console.Write("\n");
            //}
            //Console.Write("\n");

            //for (int i = 0; i <= knapsack.Capacity; i++)
            //{
            //    for (int j = 0; j <= knapsack.Items.Count; j++)
            //    {
            //        Console.Write(lastItem[j, i] + "\t");
            //    }
            //    Console.Write("\n");
            //}

            List<Item> items = new List<Item>();
            int firstIndex = knapsack.Items.Count;
            int secondIndex = knapsack.Capacity;
            while (lastItem[firstIndex, secondIndex] != -1)
            {
                if (firstIndex == lastItem[firstIndex, secondIndex])
                {
                    items.Add(knapsack.Items[firstIndex - 1]);
                    secondIndex = secondIndex - knapsack.Items[firstIndex - 1].Weight;
                    firstIndex--;
                }
                else
                {
                    firstIndex = lastItem[firstIndex, secondIndex];
                }
            }

            KnapsackResult knapsackResult = new KnapsackResult(
                items,
                sumOfValues[knapsack.Items.Count, knapsack.Capacity]);
            return knapsackResult;
        }
    }
}
