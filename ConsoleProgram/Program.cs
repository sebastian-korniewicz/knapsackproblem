﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KnapsackProblem.data.generator;
using KnapsackProblem.data.models;
using KnapsackProblem.algorithms;
namespace ConsoleProgram
{
   
    class Program
    {
        
        static void Main(string[] args)
        { 
            Generator generator = new Generator();

            DynamicProgramming dynamicProgramming = new DynamicProgramming();
            //KnapsackOnTree knapsackOnTree = new KnapsackOnTree();
            for (int i = 0; i < 10; i++)
            {
                Knapsack knapsack = generator.GenerateKnapsack(25, 50, 200, DataType.WeaklyCorrelatedInstances);
                //Console.Write(knapsack.GetInfo());

                KnapsackResult result1 = dynamicProgramming.Solve(knapsack);
                //knapsackOnTree.Solve(knapsack);
                //long memory = GC.GetTotalMemory(false);
                //GC.Collect();
                //GC.WaitForPendingFinalizers();
                Console.WriteLine(result1.GetInfo());
                //Console.WriteLine(memory / 1024 + "kB");
                Console.ReadLine();
            }
        }
    }
}
